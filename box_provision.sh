#!/bin/bash

# For CentOS 7 Ansible provisioning
sudo rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y update
sudo yum -y install python-devel
sudo yum -y install python-pip
sudo pip install --upgrade pip
sudo pip install ansible
