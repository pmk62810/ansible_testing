# README #

I created this repository to hold local testing code for ansible deployment in my homelab. Since I'm still learning the language, this serves as a sandbox environment. A more dedicated "production" repo will open later when I'm more comfortable with the code.
